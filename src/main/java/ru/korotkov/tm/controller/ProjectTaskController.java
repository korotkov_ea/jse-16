package ru.korotkov.tm.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.korotkov.tm.entity.Project;
import ru.korotkov.tm.entity.Task;
import ru.korotkov.tm.exception.NotExistElementException;
import ru.korotkov.tm.service.ProjectTaskService;

public class ProjectTaskController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(ProjectTaskController.class);

    private final ProjectTaskService projectTaskService;

    public ProjectTaskController(ProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    /**
     * Add task to project
     *
     * @param arguments - arguments of command
     * @param userId
     */
    public Task addTaskToProject(final String[] arguments, final Long userId) {
        final Long projectId = arguments.length > 0 ? Long.parseLong(arguments[0]) : null;
        final Long taskId = arguments.length > 1 ? Long.parseLong(arguments[1]) : null;
        return projectTaskService.addTaskToProject(projectId, taskId, userId);
    }

    /**
     * Add task to project
     *
     * @param arguments - arguments of command
     * @param userId
     */
    public Project removeProject(final String[] arguments, final Long userId) throws NotExistElementException {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        final String deleteTask = arguments.length > 2 ? arguments[2] : null;

        if (option == null || param == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return null;
        }

        boolean delete = deleteTask == null || deleteTask.isEmpty() ? false : deleteTask.equals("task");
        return projectTaskService.removeProject(option, param, delete, userId);
    }

}
