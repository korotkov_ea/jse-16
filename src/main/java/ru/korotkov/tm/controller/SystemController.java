package ru.korotkov.tm.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.korotkov.tm.service.SessionService;

public class SystemController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(SystemController.class);

    private final SessionService sessionService;

    public SystemController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * Get userId
     *
     * @return userId - id of user
     */
    public Long getUserId() {
        return sessionService.getUserId();
    }

    /**
     * History of commands
     */
    public void historyCommand() {
        int index = 1;
        for (final String command : sessionService.findAllCommand()) {
            logger.info("INDEX: " + index++ + " COMMAND: " + command);
        }
    }

    /**
     * Add command to history
     *
     * @param command - command
     */
    public void addCommand(final String command) {
        sessionService.addCommand(command);
    }

    /**
     * Start new session
     *
     * @param userId - id of user
     */
    public void startSession(final Long userId) {
        sessionService.startSession(userId);
    }

    /**
     * Close session
     */
    public void closeSession() {
        sessionService.closeSession();
    }

    /**
     * Welcome information
     */
    public void displayWelcome() {
        logger.info(bundle.getString("welcome"));
    }

    /**
     * Display version of program
     */
    public void displayVersion() {
        logger.info(bundle.getString("version"));
    }

    /**
     * Display information about of program
     */
    public void displayAbout() {
        logger.info(bundle.getString("about"));
    }

    /**
     * Display help
     */
    public void displayHelp() {
        logger.info(bundle.getString("help"));
    }

    /**
     * Display stub
     */
    public void displayStub(final String line) {
        logger.info(String.format(bundle.getString("stub"), line));
    }

}
