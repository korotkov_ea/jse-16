package ru.korotkov.tm.service;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.Project;
import ru.korotkov.tm.entity.Task;
import ru.korotkov.tm.exception.NotExistElementException;
import ru.korotkov.tm.repository.ProjectRepository;
import ru.korotkov.tm.repository.TaskRepository;

import java.util.ResourceBundle;

public class ProjectTaskService {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Add task to project
     *
     * @param projectId - projectId
     * @param userId
     * @param taskId - taskId
     */
    public Task addTaskToProject(final Long projectId, final Long taskId, final Long userId) {
        if (userId == null) {
            return null;
        }
        Project project = projectRepository.findById(projectId, userId);
        if (project == null || !project.getUserId().equals(userId)) {
            return null;
        }
        Task task = taskRepository.findById(taskId, userId);
        if (task == null || !task.getUserId().equals(userId)) {
            return null;
        }

        task.setProjectId(project.getId());
        return task;
    }

    /**
     * Remove project
     *
     * @param option - option of deletion
     * @param param - parameter
     * @param deleteTask - delete related task
     * @param userId
     */
    public Project removeProject(final String option, final String param, final boolean deleteTask, final Long userId) throws NotExistElementException {
        if (userId == null) {
            return null;
        }
        if (option == null || option.isEmpty()) {
            return null;
        }
        if (param == null || param.isEmpty()) {
            return null;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectRepository.removeByIndex(Integer.parseInt(param) - 1, userId);
                if (project == null) {
                    throw new NotExistElementException(bundle.getString("projectNotFoundByIndex"));
                }
                break;
            case TerminalConst.OPTION_NAME:
                project = projectRepository.removeByName(param, userId);
                if (project == null) {
                    throw new NotExistElementException(bundle.getString("projectNotFoundByName"));
                }
                break;
            case TerminalConst.OPTION_ID:
                project = projectRepository.removeById(Long.parseLong(param), userId);
                if (project == null) {
                    throw new NotExistElementException(bundle.getString("projectNotFoundById"));
                }
                break;
        }
        for (final Task task: taskRepository.findTaskByProjectId(project.getId(), userId)) {
            if (deleteTask) {
                taskRepository.removeById(task.getId(), userId);
            } else {
                task.setProjectId(null);
            }
        }
        return project;
    }

}
