package ru.korotkov.tm.service;

import ru.korotkov.tm.entity.Task;
import ru.korotkov.tm.exception.NotExistElementException;
import ru.korotkov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

public class TaskService {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name, final Long userId) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return taskRepository.create(name, userId);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Task task = create(name, userId);
        if (task != null) {
            task.setDescription(description);
        }
        return task;
    }

    public void clear(final Long userId) {
        taskRepository.clear(userId);
    }

    public Task findByIndex(final int index, final Long userId) {
        if (index < 0 || index >= taskRepository.getSize(userId)) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return taskRepository.findByIndex(index, userId);
    }

    public Task findByName(final String name, final Long userId) {
        if (name == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return taskRepository.findByName(name, userId);
    }

    public Task findById(final Long id, final Long userId) {
        if (id == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return taskRepository.findById(id, userId);
    }

    public Task removeByIndex(final int index, final Long userId) throws NotExistElementException {
        if (index < 0 || index >= taskRepository.getSize(userId)) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Task result = taskRepository.removeByIndex(index, userId);
        if (result == null) {
            throw new NotExistElementException(bundle.getString("taskNotFoundByIndex"));
        }
        return result;
    }

    public Task removeByName(final String name, final Long userId) throws NotExistElementException {
        if (name == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Task result = taskRepository.removeByName(name, userId);
        if (result == null) {
            throw new NotExistElementException(bundle.getString("taskNotFoundByName"));
        }
        return result;
    }

    public Task removeById(final Long id, final Long userId) throws NotExistElementException {
        if (id == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Task result = taskRepository.removeById(id, userId);
        if (result == null) {
            throw new NotExistElementException(bundle.getString("taskNotFoundById"));
        }
        return result;
    }

    public Task updateByIndex(final int index, final String name, final Long userId) throws NotExistElementException {
        if (index < 0 || index >= taskRepository.getSize(userId)) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Task result = taskRepository.updateByIndex(index, name, userId);
        if (result == null) {
            throw new NotExistElementException(bundle.getString("taskNotFoundByIndex"));
        }
        return result;
    }

    public Task updateByIndex(final int index, final String name, final String description, final Long userId) throws NotExistElementException {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Task task = updateByIndex(index, name, description, userId);
        task.setDescription(description);
        return task;
    }

    public Task updateById(final Long id, final String name, final Long userId) throws NotExistElementException {
        if (id == null) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Task result = taskRepository.updateById(id, name, userId);
        if (result == null) {
            throw new NotExistElementException(bundle.getString("taskNotFoundById"));
        }
        return result;
    }

    public Task updateById(final Long id, final String name, final String description, final Long userId) throws NotExistElementException {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Task task = updateById(id, name, userId);
        task.setDescription(description);
        return task;
    }

    public List<Task> findTasksByProjectId(final Long projectId, final Long userId) {
        List<Task> result = new ArrayList<>();
        if (projectId == null) {
            return result;
        }
        if (userId == null) {
            return result;
        }
        return taskRepository.findTaskByProjectId(projectId, userId);
    }

    public Task removeTaskFromProject(final Long taskId, final Long userId) {
        Task task = findById(taskId, userId);
        if (task != null) {
            task.setProjectId(null);
        }
        return task;
    }

    public List<Task> findAll(final Long userId) {
        if (userId == null) {
            return new ArrayList<>();
        }
        return taskRepository.findAll(userId);
    }

    public List<Task> findAll(final Long userId, Comparator<Task> comporator) {
        if (comporator == null) {
            return new ArrayList<>();
        }
        List<Task> tasks = taskRepository.findAll(userId);
        if (tasks == null) {
            return null;
        }
        tasks.sort(comporator);
        return tasks;
    }

}